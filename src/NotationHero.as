package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.text.Font;
	
	import view.ViewController;
	
	public class NotationHero extends Sprite
	{
		[Embed(source="fonts/Musical.ttf", fontName ="pafoid Musical", mimeType="application/x-font-truetype", embedAsCFF="false")]
		private var musicalFont:Class;
		
		private var _appFacade:AppFacade;
		private var _viewController:ViewController;
		
		public function NotationHero()
		{
			super();
			
			// support autoOrients
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			init();
		}
		
		private function init():void{
			initFonts();
			
			_appFacade = new AppFacade(stage);
			_viewController = new ViewController(_appFacade);
		}
		
		private function initFonts():void
		{
			Font.registerFont(musicalFont);
		}
	}
}