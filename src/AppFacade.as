package
{
	import flash.display.Stage;
	
	import managers.ScoreManager;
	import managers.Ticker;

	public class AppFacade
	{
		public const SUCCESS_SCORE:int = 100;
		public const FAILURE_SCORE:int = -50;
		
		private var _stage:Stage;
		private var _ticker:Ticker;
		private var _scoreManager:ScoreManager;
		
		public function AppFacade(stage:Stage)
		{
			_stage = stage;
			_ticker = new Ticker(stage);
			_scoreManager = new ScoreManager();
		}
		
		//Getters/Setters
		public function get stage():Stage
		{
			return _stage;
		}
		
		public function get scaleX():Number{
			return 1920/stage.fullScreenWidth;
		}
		
		public function get scaleY():Number{
			return 1080/stage.fullScreenHeight;
		}

		public function get scoreManager():ScoreManager
		{
			return _scoreManager;
		}

		public function get ticker():Ticker
		{
			return _ticker;
		}

		public function set ticker(value:Ticker):void
		{
			_ticker = value;
		}


	}
}