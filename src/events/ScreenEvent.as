package events
{
	import flash.events.Event;
	
	public class ScreenEvent extends Event
	{
		public static const CHANGE_SCREEN:String = "changeScreen";
		public static const SHOW_POPUP:String = "showPopup";
		public static const HIDE_POPUP:String = "hidePopup";
		
		public var screenName:String;
		
		public function ScreenEvent(type:String, screenName:String)
		{
			super(type, true, false);
			
			this.screenName = screenName;
		}
	}
}