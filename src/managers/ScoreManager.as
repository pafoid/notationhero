package managers
{
	import com.pafoid.sharedObject.SharedObjectManager;
	
	import model.SharedObjectKeys;

	public class ScoreManager
	{
		public const RIGHT_GUESS:int = 1;
		public const WRONG_GUESS:int = 0;
		
		private var _currentScore:Number = 0;
		private var _bestScore:Number = 0;
		
		public function ScoreManager()
		{
			_bestScore = SharedObjectManager.instance.getValue(SharedObjectKeys.BEST_SCORE);
		}

		//Getters/Setters
		public function get currentScore():Number
		{
			return _currentScore;
		}

		public function set currentScore(value:Number):void
		{
			_currentScore = Math.max(0, value);
		}

		public function get bestScore():Number
		{
			return _bestScore;
		}

		public function set bestScore(value:Number):void
		{
			_bestScore = value;
			SharedObjectManager.instance.save(SharedObjectKeys.BEST_SCORE, value);
		}

	}
}