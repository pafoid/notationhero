package managers
{
	import com.pafoid.array.ArrayUtils;
	
	import flash.display.Stage;
	import flash.events.Event;

	public class Ticker
	{
		public var isTicking:Boolean;
		
		private var _stage:Stage;
		private var _registeredItems:Array = [];	
		
		public function Ticker(stage:Stage, startTickingAutomatically:Boolean = true)
		{
			_stage = stage;
			
			if(startTickingAutomatically)
				startTicking();
		}
		
		private function tick(event:Event):void{
			for each (var item:* in _registeredItems) 
			{
				try{
					item.tick();
				}catch(e:Error){}
			}
			
		}
		
		public function startTicking():void{
			_stage.addEventListener(Event.ENTER_FRAME, tick, false, 0, true);
			isTicking = true;
		}
		
		public function stopTicking():void{
			_stage.removeEventListener(Event.ENTER_FRAME, tick);
			isTicking = false;
		}
		
		public function registerForTick(item:*):void{
			_registeredItems.push(item);
		}
		
		public function unregisterForTick(item:*):void{
			ArrayUtils.removeItem(_registeredItems, item);
		}
		
		public function destroy():void{
			stopTicking();
			_stage = null;
			_registeredItems = null;
		}
	}
}