package com.pafoid.music
{
	import com.pafoid.display.BitmapUtils;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.PixelSnapping;
	import flash.geom.ColorTransform;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;

	public class Note
	{
		public static const DO:int = 0;
		public static const RE:int = 1;
		public static const MI:int = 2;
		public static const FA:int = 3;
		public static const SOL:int = 4;
		public static const LA:int = 5;
		public static const SI:int = 6;
		
		public static const A:int = 5;
		public static const B:int = 6;
		public static const C:int = 0;
		public static const D:int = 1;
		public static const E:int = 2;
		public static const F:int = 3;
		public static const G:int = 4;
		
		public static const NOTE_STRING_EN:Array = ["C", "D", "E", "F", "G", "A", "B"];
		public static const NOTE_STRING_FR:Array = ["Do", "Ré", "Mi", "Fa", "Sol", "La", "Si"];
		
		public static const NOTE_LETTERS:Array = [""];
		
		private var _id:String;
		private var _position:int;
		private var _length:Number;
		private var _dotted:Boolean;
		private var _guessValue:int = -1;
		
		private var _reprensentation:Bitmap;
		
		public function Note(position:int, length:Number = 1, dotted:Boolean = false, id:String = "")
		{
			if(position > 6 || position < 0)
				throw new Error("A musical note's position must be between 0 and 6. It was set to :"+position);
			
			if(length <=0)
				throw new Error("A musical note's length has to be more than 0.");
			
			
			_position = position;
			_length = length;
			_dotted = dotted;
			_id = (id != "") ? id : toFrString(true)+"-"+new Date().time.toString();
			
			trace(toFrString(true));
		}
		
		//Public methods
		public function get id():String
		{
			return _id;
		}

		public function set id(value:String):void
		{
			_id = value;
		}

		public function toEnString(showLength:Boolean = false):String{
			return (showLength) ? NOTE_STRING_EN[_position] + " - " + NoteLength.toEnString(_length) : NOTE_STRING_EN[_position];
		}
		
		public function toFrString(showLength:Boolean = false):String{
			return (showLength) ? NOTE_STRING_FR[_position] + " - " + NoteLength.toFrString(_length) : NOTE_STRING_FR[_position];
		}

		//Getters/Setters
		public function get position():int
		{
			return _position;
		}

		public function set position(value:int):void
		{
			_position = value;
		}

		public function get dotted():Boolean
		{
			return _dotted;
		}

		public function set dotted(value:Boolean):void
		{
			_dotted = value;
		}

		public function get length():Number
		{
			return _length;
		}
		
		public function set length(value:Number):void
		{
			_length = value;
		}
		
		public function get reprensentation():Bitmap
		{
			if(_reprensentation)
				return _reprensentation;
			else
				return getReprensentation();
		}
		
		public function set reprensentation(value:Bitmap):void
		{
			_reprensentation = value;
		}
		
		private function getReprensentation():Bitmap
		{
			if(!_reprensentation){
				var textFormat:TextFormat = new TextFormat("pafoid Musical", 365, 0x000000);
				
				var tf:TextField = new TextField();
				tf.autoSize = TextFieldAutoSize.LEFT;
				tf.embedFonts = true;
				tf.defaultTextFormat = textFormat;
				tf.text = NoteLength.representation(_length);
				tf.height = tf.textHeight;
				
				var bmd:BitmapData = new BitmapData(tf.width, tf.height, true, 0xFFFFFF);
				var rect:Rectangle = bmd.getColorBoundsRect(0xFFFFFF, 0xFF0000, true);
				bmd.draw(tf, null, null, null, null, true);
				
				_reprensentation = new Bitmap(BitmapUtils.trimAlpha(bmd), PixelSnapping.NEVER, true);
			}
			
			return _reprensentation;
		}
		
		public function changeColor(color:uint):void{
			var ct:ColorTransform = new ColorTransform();
			ct.color = color;
			reprensentation.transform.colorTransform = ct;
		}
		
		public function get scrollX():Number{
			return reprensentation.x + reprensentation.width;
		}
				
		public static function generateRandomNote(randomLength:Boolean = false):Note{
			var length:Number = (randomLength) ? NoteLength.generateRandomLength() : 1;
			var note:Note = new Note(Math.round(Math.random() * 6), length);
			return note;
		}

		public function get guessValue():int
		{
			return _guessValue;
		}

		public function set guessValue(value:int):void
		{
			_guessValue = value;
		}

	}
}