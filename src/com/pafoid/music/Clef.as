package com.pafoid.music
{
	import com.pafoid.display.BitmapUtils;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.PixelSnapping;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;

	public class Clef
	{
		public static const SOL:uint = 0;
		public static const DO:uint = 1;
		public static const FA:uint = 2;
		
		public static const TREBLE:uint = 0;
		public static const BASS:uint = 1;
		public static const ALTO:uint = 2;
		public static const TENOR:uint = 3;
		
		public static const CLEF_STRING_EN:Array = ["G", "C", "F"];
		public static const CLEF_STRING_FR:Array = ["Sol", "Do", "Fa"];
		
		private var _type:uint;
		private var _representation:Bitmap;
		
		public function Clef(type:uint = SOL)
		{
			_type = type;
		}
		
		public function get representation():Bitmap
		{
			if(_representation)
				return _representation;
			else
				return getRepresentation();
		}
		
		public function set representation(value:Bitmap):void
		{
			_representation = value;
		}
		
		private function getRepresentation():Bitmap
		{
			if(!_representation){
				var textSize:Number = (typeString == "?") ? 352 : 365;
				var textFormat:TextFormat = new TextFormat("pafoid Musical", textSize, 0x000000);
				
				var tf:TextField = new TextField();
				tf.autoSize = TextFieldAutoSize.LEFT;
				tf.embedFonts = true;
				tf.defaultTextFormat = textFormat;
				tf.text = typeString;
				tf.height = tf.textHeight;
				
				var bmd:BitmapData = new BitmapData(tf.width, tf.height, true, 0xFFFFFF);
				var rect:Rectangle = bmd.getColorBoundsRect(0xFFFFFF, 0xFF0000, true);
				bmd.draw(tf, null, null, null, null, true);
				
				_representation = new Bitmap(BitmapUtils.trimAlpha(bmd), PixelSnapping.NEVER, true);
			}
			
			return _representation;
		}
		
		public function get typeString():String{
			switch(_type){
				case SOL:
					return "&";
					break;
				case DO:
					return "B";
					break;
				case FA:
					return "?";
					break;
			}
			
			return "";
		}

		public function get type():uint
		{
			return _type;
		}

		public function set type(value:uint):void
		{
			_type = value;
		}

	}
}