package com.pafoid.music
{
	public class NoteLength
	{
		//FR
		public static const RONDE:Number = 4;
		public static const BLANCHE:Number = 2;
		public static const NOIRE:Number = 1;
		public static const CROCHE:Number = 0.5;
		public static const DOUBLE_CROCHE:Number = 0.25;
		public static const TRIPLE_CROCHE:Number = 0.125;
		
		//EN
		public static const WHOLE:Number = 4;
		public static const HALF:Number = 2;
		public static const QUARTER:Number = 1;
		public static const EIGHTH:Number = 0.5;
		public static const SIXTEENTH:Number = 0.25;
		public static const THIRTY_SECOND:Number = 0.125;
		
		public static const POSSIBLE_LENGTH:Array = [WHOLE, HALF, QUARTER, EIGHTH, SIXTEENTH, THIRTY_SECOND];
		
		public static function representation(length:Number):String{
			var representation:String;
			switch(length){
				case RONDE :
					representation = "w";
					break;
				case BLANCHE :
					representation = "h";
					break;
				case NOIRE :
					representation = "q";
					break;
				case CROCHE :
					representation = "e";
					break;
				case DOUBLE_CROCHE :
					representation = "x";
					break;
				case TRIPLE_CROCHE :
					representation = "r";
					break;
			}
			
			return representation;
		}
		
		public static function generateRandomLength():Number{
			return POSSIBLE_LENGTH[Math.round(Math.random() * (POSSIBLE_LENGTH.length-1))];
		}
		
		public static function toEnString(length:Number):String{
			var enString:String;
			switch(length){
				case WHOLE :
					enString = "whole";
					break;
				case HALF :
					enString = "half";
					break;
				case QUARTER :
					enString = "quarter";
					break;
				case EIGHTH :
					enString = "eighth";
					break;
				case SIXTEENTH :
					enString = "sixteenth";
					break;
				case THIRTY_SECOND :
					enString = "thirty second";
					break;
			}
			
			return enString;
		}
		
		public static function toFrString(length:Number):String{
			var frString:String;
			switch(length){
				case RONDE :
					frString = "ronde";
					break;
				case BLANCHE :
					frString = "blanche";
					break;
				case NOIRE :
					frString = "noire";
					break;
				case CROCHE :
					frString = "croche";
					break;
				case DOUBLE_CROCHE:
					frString = "double croche";
					break;
				case TRIPLE_CROCHE:
					frString = "triple croche";
					break;
			}
			
			return frString;
		}
	}
}