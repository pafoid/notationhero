package com.pafoid.music
{
	import com.pafoid.array.ArrayUtils;
	import com.pafoid.display.Color;
	import com.pafoid.geometry.SquareCreator;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.ColorTransform;
	
	public class ScrollingStaff extends Staff
	{
		private var _speed:Number = 12;
		private var _isScrolling:Boolean = true;
		private var _currentNote:Note;
		private var _guessableWidth:Number;
		private var _guessableNotes:Array = [];
		
		private var _guessableLimit:Number;
		private var _showDebugRect:Boolean;
		private var _debugRect:Sprite;
		
		private var _mask:Sprite;
		
		public function ScrollingStaff(appFacade:AppFacade, color:uint=0x000000, guessableWidth:Number = -1, showDebugRect:Boolean = true)
		{
			super(appFacade, color);
			
			_guessableWidth = guessableWidth;
			_showDebugRect = showDebugRect;
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		public function get guessableNotes():Array
		{
			return _guessableNotes;
		}

		protected function onAddedToStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			_appFacade.ticker.registerForTick(this);
			
			_guessableWidth = (_guessableWidth == -1) ? stage.fullScreenWidth * 0.7 : _guessableWidth;
			_guessableLimit = stage.fullScreenWidth - _guessableWidth;
			
			if(_showDebugRect)
				showDebugRect();
			
			//Mask
			var position:Number = _clef.representation.x + _clef.representation.width;
			var maskWidth:Number = stage.fullScreenWidth - position;
			_mask = SquareCreator.createSquare(new Sprite(), position, height/-2, maskWidth, height*2, 0, 0, 0xFF0000, 0.2, 0, this);
		}
		
		public function tick():void{
			//Scroll notes
			if(_isScrolling){
				for each (var noteBmp:Bitmap in _notesBitmaps) 
				{
					noteBmp.x -= speed;
				}
				
				for each (var note:Note in _notes) 
				{
					//Guessable notes
					if(_guessableNotes.indexOf(note) > -1){
						if(note.scrollX < _guessableLimit){
							ArrayUtils.removeItem(_guessableNotes, note);
							
							if(note.guessValue < 0){
								note.changeColor(Color.RED);
								_appFacade.scoreManager.currentScore += _appFacade.FAILURE_SCORE;
							}
						}
					}else{
						if(note.scrollX < stage.fullScreenWidth && note.scrollX > _guessableLimit){
							_guessableNotes.push(note);
						}
					}
					
					//Remove notes from stage
					if(note.reprensentation.x < -note.reprensentation.width){
						removeChild(note.reprensentation);
						ArrayUtils.removeItem(_notes, note);
					}
					
					_currentNote = _guessableNotes[0];
				}
			}
		}
		
		override public function addNote(note:Note):void{
			var offset:Number = 32;
			
			_notesBitmaps.push(note.reprensentation);
			_notes.push(note);
			
			var scale:Number = Math.max(1/_appFacade.scaleX, 1/_appFacade.scaleY);
			note.reprensentation.scaleX = scale;
			note.reprensentation.scaleY = scale;
			
			note.reprensentation.y = getPositionByNote(note) - note.reprensentation.height + offset;
			var previousNoteWidth:Number = (_notes.length - 2 > 0) ? _notes[_notes.length - 2].reprensentation.width : 120;
			trace("previousNoteWidth", previousNoteWidth);
			note.reprensentation.x = _appFacade.stage.fullScreenWidth + ((_notes.length-1) * SPACE) + previousNoteWidth + SPACE;
			note.reprensentation.mask = _mask;
			
			addChild(note.reprensentation);
		}
		
		public function removeCurrentNote():void{
			if(_currentNote && contains(_currentNote.reprensentation))
				removeChild(_currentNote.reprensentation);
			
			_currentNote = defineCurrentNote();
		}
		
		public function guessCurrentNoteByName(noteName:String):Boolean{
			trace("Guessed : "+noteName);
			trace("Actual name : "+_currentNote.toFrString());
			
			var success:Boolean = (noteName == _currentNote.toFrString());
			_currentNote.guessValue = (success) ? _appFacade.scoreManager.RIGHT_GUESS : _appFacade.scoreManager.WRONG_GUESS;
			return success;
		}
		
		public function guessCurrentNoteById(id:String):Boolean{
			return (_currentNote.id == id);
		}
		
		public function colorCurrentNote(color:uint):void{
			var colorTransform:ColorTransform = new ColorTransform();
			colorTransform.color = color;
			_currentNote.reprensentation.transform.colorTransform = colorTransform;
		}
		
		public function hideDebugRect():void{
			if(_debugRect){
				removeChild(_debugRect);
				_debugRect = null;
			}
		}
		
		public function destroy():void{
			_appFacade.ticker.unregisterForTick(this);
		}
		
		//Private methods
		private function defineCurrentNote():Note{
			_currentNote = _guessableNotes[0];
			return _currentNote;
		}
		
		private function showDebugRect():void{
			_debugRect = SquareCreator.createSquare(_debugRect, _guessableLimit, 0, stage.fullScreenWidth - _guessableLimit, _lines[4].y - _lines[0].y, 0, 0, 0xFF03F0, 0.5, 0, this);
			addChild(_debugRect);
		}
		
		//Getters/Setters
		public function get speed():Number
		{
			return _speed;
		}

		public function set speed(value:Number):void
		{
			_speed = value;
		}

		public function get currentNote():Note
		{
			if(!_currentNote)
				_currentNote = _guessableNotes[0];
			return _currentNote;
		}
	}
}