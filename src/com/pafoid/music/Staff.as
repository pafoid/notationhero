package com.pafoid.music
{
	import flash.display.Bitmap;
	import flash.display.CapsStyle;
	import flash.display.Sprite;

	public class Staff extends Sprite
	{
		public static const SPACE:Number = 120;
		
		protected var _appFacade:AppFacade;
		protected var _color:uint;
		protected var _clef:Clef;
		protected var _clefType:uint;
		
		protected var _notesBitmaps:Array = [];
		protected var _notes:Array = [];
		protected var _lines:Array = [];
		
		public function Staff(appFacade:AppFacade, color:uint = 0x000000, clefType:uint = 0)
		{
			_appFacade = appFacade;
			_color = color;
			_clefType = clefType;
			init();
		}
		
		protected function init():void{
			//Lines
			var spacing:Number = (_appFacade.stage.fullScreenHeight / 3) / 4;
			
			for (var i:int = 0; i < 5; i++) 
			{
				var line:Sprite = new Sprite();
				line = generateLine();	
				line.y = i*spacing;
				addChild(line);
				_lines.push(line);
			}
			
			//Clef
			_clef = new Clef(_clefType);
			
			var scale:Number = Math.max(1/_appFacade.scaleX, 1/_appFacade.scaleY);
			_clef.representation.scaleX = scale;
			_clef.representation.scaleY = scale;

			_clef.representation.x = 10;
			_clef.representation.y = getPositionByClef(_clef);
			addChild(_clef.representation);
		}
		
		private function generateLine():Sprite{
			var line:Sprite = new Sprite();
			
			line.graphics.lineStyle(2, _color, 1, false, "normal", CapsStyle.SQUARE);
			line.graphics.moveTo(0, 0);
			line.graphics.lineTo(_appFacade.stage.fullScreenWidth, 0);
			line.mouseEnabled = line.mouseChildren = false;
			
			return line;
		}
		
		protected function getPositionByNote(note:Note):Number{
			var position:Number;
			
			switch(note.position)
			{
				case Note.DO:
					position = _lines[1].y + (_lines[2].y - _lines[1].y) / 2;
					break;
				case Note.RE:
					position = _lines[1].y;
					break;
				case Note.MI:
					position = _lines[4].y;
					//position = _lines[0].y + (_lines[1].y - _lines[0].y) / 2;
					break;
				case Note.FA:
					position = _lines[0].y;
					//Or position = _lines[3].y + (_lines[4].y - _lines[3].y) / 2;
					break;
				case Note.SOL:
					position = _lines[3];
					break;
				case Note.LA:
					position = _lines[2].y + (_lines[3].y - _lines[2].y) / 2;
					break;
				case Note.SI:
					position = _lines[2].y;
					break;
			}
			
			return position;
		}
		
		protected function getPositionByClef(clef:Clef):Number{
			var position:Number;
			var scale:Number = Math.max(1/_appFacade.scaleX, 1/_appFacade.scaleY);
			
			switch(clef.type)
			{
				case Clef.SOL:
					position = _lines[4].y - (clef.representation.height * 0.77);
					break;
				case Clef.DO:
					position = _lines[4].y - clef.representation.height
					break;
				case Clef.FA:
					position = _lines[0].y;
					break;
			}
			
			return position;
		}
		
		public function addNote(note:Note):void{
			var offset:Number = 32;
			
			var bmp:Bitmap = note.reprensentation;
			_notesBitmaps.push(bmp);
			bmp.y = getPositionByNote(note) - bmp.height + offset;
			bmp.x = getNextPosition();
			addChild(bmp);
		}
		
		protected function getNextPosition():Number{
			var position:Number = -SPACE;
			
			for each (var note:Bitmap in _notesBitmaps) 
			{
				position += note.width + SPACE;
			}
			
			return position;
		}
		
		//Getters/Setters
		public function get lines():Array
		{
			return _lines;
		}

		public function set lines(value:Array):void
		{
			_lines = value;
		}

		public function get clef():Clef
		{
			return _clef;
		}

		public function set clef(value:Clef):void
		{
			_clef = value;
		}

	}
}