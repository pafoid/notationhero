package view
{
	public class TransitionConstants
	{
		public static const SIMPLE_SWAP:String = "simpleSwap";
		
		public static const SLIDE_FROM_LEFT:String = "slideFromLeft";
		public static const SLIDE_FROM_RIGHT:String = "slideFromRight";
		public static const SLIDE_FROM_BOTTOM:String = "slideFromBottom";
		public static const SLIDE_FROM_TOP:String = "slideFromTop";
	}
}