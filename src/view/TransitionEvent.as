package view
{
	import flash.events.Event;
	
	public class TransitionEvent extends Event
	{
		public static const TRANSITION_CONTROLLER_INITIALIZED:String = "transitionControllerInitialized";
		public static const TRANSITION_COMPLETE:String = "transitionComplete";
		public static const TRANSITION_STARTED:String = "transitionStarted";
		
		public function TransitionEvent(type:String)
		{
			super(type, true, false);
		}
	}
}