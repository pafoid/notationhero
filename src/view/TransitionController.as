package view
{
	import com.greensock.TweenLite;
	
	import flash.events.EventDispatcher;

	public class TransitionController extends EventDispatcher
	{
		private var _appFacade:AppFacade;
		private var _type:String;
		private var _ease:Function;
		private var _duration:Number;
		private var _oldScreen:Screen;
		private var _newScreen:Screen;
		private var _destroyOldScreen:Boolean;
		
		public function TransitionController(appFacade:AppFacade)
		{
			_appFacade = appFacade;
			dispatchEvent(new TransitionEvent(TransitionEvent.TRANSITION_CONTROLLER_INITIALIZED));
		}
		
		public function startTransition(type:String, oldScreen:Screen, newScreen:Screen, duration:Number = 0, ease:Function = null, destroyOldScreen:Boolean = true):void{
			_type = type;
			_duration = duration;
			_oldScreen = oldScreen;
			_newScreen = newScreen;
			_ease = ease;
			_destroyOldScreen = destroyOldScreen;
			
			dispatchEvent(new TransitionEvent(TransitionEvent.TRANSITION_STARTED));
			
			switch(_type){
				case TransitionConstants.SIMPLE_SWAP :
					simpleSwap();
					break;
				case TransitionConstants.SLIDE_FROM_RIGHT :
					slideFromRight();
					break;
			}
		}
		
		//Transition methods
		private function simpleSwap():void{
			_appFacade.stage.addChild(_newScreen);
			
			if(_oldScreen && _appFacade.stage.contains(_oldScreen))
				_appFacade.stage.removeChild(_oldScreen);
			
			if(_destroyOldScreen && _oldScreen){
				_oldScreen.destroy();
				_oldScreen = null;
			}
			
			dispatchEvent(new TransitionEvent(TransitionEvent.TRANSITION_COMPLETE));
		}
		
		private function slideFromRight():void{
			_newScreen.x = _appFacade.stage.fullScreenWidth;
			
			if(!_appFacade.stage.contains(_newScreen))
				_appFacade.stage.addChild(newScreen);
			
			TweenLite.to(_newScreen, _duration, {x:0, ease:_ease});
			TweenLite.to(_oldScreen, _duration, {x:-_appFacade.stage.fullScreenWidth, ease:_ease, onComplete:onTransitionComplete});
		}
		
		private function onTransitionComplete():void{
			dispatchEvent(new TransitionEvent(TransitionEvent.TRANSITION_COMPLETE));
			if(_destroyOldScreen){
				_appFacade.stage.removeChild(_oldScreen);
				_oldScreen.destroy();
				_oldScreen = null;
			}
		}

		//Getters/Setters
		public function get type():String
		{
			return _type;
		}

		public function set type(value:String):void
		{
			_type = value;
		}

		public function get oldScreen():Screen
		{
			return _oldScreen;
		}

		public function set oldScreen(value:Screen):void
		{
			_oldScreen = value;
		}

		public function get newScreen():Screen
		{
			return _newScreen;
		}

		public function set newScreen(value:Screen):void
		{
			_newScreen = value;
		}
		
		public function destroy():void{
			_oldScreen = null;
			_newScreen = null;
			_type = null;
		}

	}
}