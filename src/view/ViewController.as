package view
{
	import com.greensock.easing.Back;
	
	import flash.events.EventDispatcher;
	
	import events.ScreenEvent;
	
	public class ViewController extends EventDispatcher
	{
		private var _appFacade:AppFacade;
		private var _currentScreen:Screen;
		private var _previousScreen:Screen;
		private var _transitionController:TransitionController;
		
		public function ViewController(appFacade:AppFacade)
		{
			_appFacade = appFacade;
			_transitionController = new TransitionController(_appFacade);
			changeScreen(ScreenConstants.HOME_SCREEN);
		}
		
		//Private methods
		private function changeScreen(screenName:String):void{
			var transition:String;
			var transitionDuration:Number = 0;
			var ease:Function;
			
			_previousScreen = _currentScreen;
			
			switch(screenName){
				case ScreenConstants.HOME_SCREEN:
					_currentScreen = new HomeScreen(_appFacade);
					transition = TransitionConstants.SIMPLE_SWAP;
					break;
				case ScreenConstants.MODE1_SCREEN:
					_currentScreen = new Mode1Screen(_appFacade);
					transition = TransitionConstants.SLIDE_FROM_RIGHT;
					transitionDuration = 0.6;
					break;
			}
			
			addEventListeners();
			
			_transitionController.startTransition(transition, _previousScreen, _currentScreen, transitionDuration, ease);
		}
		
		private function addEventListeners():void{
			_currentScreen.addEventListener(ScreenEvent.CHANGE_SCREEN, screenEventHandler, false, 0, true);
			_transitionController.addEventListener(TransitionEvent.TRANSITION_COMPLETE, _currentScreen.onTransitionComplete, false, 0, true);
		}
		
		private function removeEventListeners():void{
			_currentScreen.removeEventListener(ScreenEvent.CHANGE_SCREEN, screenEventHandler);
			_transitionController.removeEventListener(TransitionEvent.TRANSITION_COMPLETE, _currentScreen.onTransitionComplete);
		}
		
		private function showPopup(screenName:String):void{
			
		}
		
		private function hidePopup(screenName:String):void{
			
		}
		
		//Handlers
		private function screenEventHandler(event:ScreenEvent):void{
			switch(event.type){
				case ScreenEvent.CHANGE_SCREEN:
					changeScreen(event.screenName);
					break;
				case ScreenEvent.SHOW_POPUP:
					showPopup(event.screenName);
					break;
				case ScreenEvent.HIDE_POPUP:
					hidePopup(event.screenName);
					break;
			}
		}
	}
}