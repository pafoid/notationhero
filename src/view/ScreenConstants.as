package view
{
	public class ScreenConstants
	{
		public static const HOME_SCREEN:String = "homeScreen";
		public static const MODE1_SCREEN:String = "mode1Screen";
		public static const OPTIONS_SCREEN:String = "optionsScreen";
		
		public static const PAUSE_MENU:String = "pauseMenu";
	}
}