package view.popup
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.text.TextField;
	
	public class Popup extends Sprite
	{
		protected var _appFacade:AppFacade;
		
		protected var _titleTextField:TextField;
		
		public function Popup(appFacade:AppFacade)
		{
			super();
			_appFacade = appFacade;
			
		}
		
		protected function init():void{
			
		}
		
		//Public methods
		public function show():void{
			visible = true;
		}
		
		public function hide():void{
			visible = false;
		}
		
		public function center(item:DisplayObject):void{
			centerHorizontaly(item);
			centerVerticaly(item);
		}
		
		public function centerHorizontaly(item:DisplayObject):Number{
			item.x = width/2 - item.width/2;
			return item.x;
		}
		
		public function centerVerticaly(item:DisplayObject):Number{
			item.y = height/2 - item.height/2;
			return item.y;
		}
		
		public function destroy():void{
			
		}
	}
}