package view.popup
{
	import com.pafoid.display.ButtonFactory;
	import com.pafoid.display.TextFieldFactory;
	import com.pafoid.geometry.SquareCreator;
	import com.pafoid.string.StringUtils;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class GameResultsPopup extends Popup
	{
		private var _scoreTextField:TextField;
		private var _bestScoreTextField:TextField;
		private var _msgTextField:TextField;
		private var _replayButton:Sprite;
		
		public function GameResultsPopup(appFacade:AppFacade)
		{
			super(appFacade);
			init();
		}
		
		override protected function init():void{
			//BG
			SquareCreator.createSquare(this, 0, 0, 400, 250, 1, 0x000000, 0xFFFFFF, 1, 1);
			
			//Text
			_titleTextField = TextFieldFactory.createTextField("Game Over !", 0, 0, new TextFormat(null, 20), false, "left", this);
			centerHorizontaly(_titleTextField);
			_titleTextField.y = 5;
			
			_scoreTextField = TextFieldFactory.createTextField("", 5, 25, new TextFormat(null, 15), false, "left", this);
			var score:Number = _appFacade.scoreManager.currentScore;
			_scoreTextField.text = "Score : "+_appFacade.scoreManager.currentScore.toString();
			
			//Buttons
			_replayButton = ButtonFactory.createButton(_replayButton, this, "Replay", 100, 30, 0, 200, 15, true, onClickReplay);
			centerHorizontaly(_replayButton);
		}
		
		//Handlers
		private function onClickReplay(event:MouseEvent):void{
			
		}
		
		//Public methods
		override public function destroy():void{
			super.destroy();
		}
	}
}