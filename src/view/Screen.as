package view
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	
	import view.popup.Popup;
	
	public class Screen extends Sprite
	{
		protected var _appFacade:AppFacade;
		protected var _popup:Popup;
		
		public function Screen(appFacade:AppFacade)
		{
			_appFacade = appFacade;
		}
		
		public function onTransitionComplete(event:TransitionEvent):void{
			
		}
		
		public function showPopup():void{
			if(!_popup){
				throw new Error("Popup is null");
			}
				
			if(!contains(_popup)){
				addChild(_popup);
			}
			
			center(_popup);
			_popup.show();	
		}
		
		public function hidePopup():void{
			if(contains(_popup))
				removeChild(_popup);
			
			_popup.hide();
			_popup.destroy();
			_popup = null;
		}
		
		public function center(item:DisplayObject):void{
			centerHorizontaly(item);
			centerVerticaly(item);
		}
		
		public function centerHorizontaly(item:DisplayObject):Number{
			item.x = _appFacade.stage.fullScreenWidth/2 - item.width/2;
			return item.x;
		}
		
		public function centerVerticaly(item:DisplayObject):Number{
			item.y = _appFacade.stage.fullScreenHeight/2 - item.height/2;
			return item.y;
		}
		
		public function destroy():void{
			_appFacade = null;
			
			if(_popup){
				_popup.destroy();
				_popup = null;
			}
		}
	}
}