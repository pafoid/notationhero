package view
{
	import com.pafoid.display.ButtonFactory;
	import com.pafoid.display.Color;
	import com.pafoid.display.TextFieldFactory;
	import com.pafoid.music.Note;
	import com.pafoid.music.ScrollingStaff;
	import com.pafoid.string.StringUtils;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	
	import view.popup.GameResultsPopup;

	public class Mode1Screen extends GameScreen
	{
		private const SCORE:String = "Score : {0}";
		private const TIME:String = "Time : {0}";
		private const TIME_LIMIT:Number = 10;
		
		//UI
		private var _scoreTextField:TextField;
		private var _timeTextField:TextField;
		private var _noteButtons:Array = [];
		private var _staff:ScrollingStaff;
		
		//Game
		private var _spawnInterval:uint;
		private var _speed:Number = 0.2;//By Seconds
		private var _gameStartTime:Number;
		
		public function Mode1Screen(appFacade:AppFacade)
		{
			super(appFacade);
			name = ScreenConstants.MODE1_SCREEN;
			
			init();
		}
		
		private function init():void{
			//Staff
			_staff = new ScrollingStaff(_appFacade);
			_staff.y = 288;//_staff.height + 30;
			addChild(_staff);
			
			//Buttons
			var padding:Number = 20 * 1/_appFacade.scaleX;
			var buttonPosition:Number = ((_appFacade.stage.fullScreenWidth -  padding*2)/ Note.NOTE_STRING_FR.length);
			for (var i:int = 0; i < Note.NOTE_STRING_FR.length; i++) 
			{
				var button:Sprite = ButtonFactory.createButton(new Sprite(), this, Note.NOTE_STRING_FR[i], 200, 60, 0, 0, 40, true, onClickNoteButton);
				button.name = "button_"+Note.NOTE_STRING_FR[i];
				button.scaleX = 1/_appFacade.scaleX;
				button.scaleY = 1/_appFacade.scaleY;
				
				button.x = (buttonPosition * i) + padding*2;
				button.y = 925 * 1/_appFacade.scaleY;
				_noteButtons.push(button);
			}
			
			//UI 
			var tf:TextFormat = new TextFormat(null, 40);
			_scoreTextField = TextFieldFactory.createTextField("Score :", 0, 10, tf);
			_scoreTextField.scaleX = 1/_appFacade.scaleX;
			_scoreTextField.scaleY = 1/_appFacade.scaleY;
			_scoreTextField.x = padding;
			
			_timeTextField = TextFieldFactory.createTextField("Time : 0:00", 0, 10, tf);
			_timeTextField.scaleX = 1/_appFacade.scaleX;
			_timeTextField.scaleY = 1/_appFacade.scaleY;
			_timeTextField.x = _appFacade.stage.fullScreenWidth - (_timeTextField.width + padding);
			
			addChild(_scoreTextField);
			addChild(_timeTextField);
		}
		
		//Private methods
		private function startScrolling():void{
			_appFacade.ticker.registerForTick(this);
			_spawnInterval = setInterval(addRandomNote, 1000/_speed);
			_gameStartTime = new Date().time;
		}
		
		private function stopScrolling():void{
			_appFacade.ticker.unregisterForTick(this);
			clearInterval(_spawnInterval);
		}
		
		private function addRandomNote():void{
			var note:Note = Note.generateRandomNote(true);
			trace("Adding random note : "+note.toFrString());
			_staff.addNote(note);
		}
		
		private function showGameResults():void{
			_appFacade.ticker.unregisterForTick(this);
			
			trace("showGameResults");
			trace("Score : ", _appFacade.scoreManager.currentScore);
			trace("Best Score : ", _appFacade.scoreManager.bestScore);
			if(_appFacade.scoreManager.currentScore >= _appFacade.scoreManager.bestScore){
				trace("New Record!");
			}
			
			//Popup
			_popup = new GameResultsPopup(_appFacade);
			showPopup();
		}
		
		//Public Methods
		public function tick():void{
			//Update score
			_scoreTextField.text = StringUtils.replaceNumberedTokens(SCORE, [_appFacade.scoreManager.currentScore]);
			
			//Update time
			var gameLength:Number = new Date().time - _gameStartTime;
			var timeString:String = StringUtils.formatTime(gameLength, StringUtils.MINUTES);			
			_timeTextField.text = StringUtils.replaceNumberedTokens(TIME, [timeString]);
			
			//Reached time limit
			if(gameLength/1000 >= TIME_LIMIT){
				stopScrolling();
				showGameResults();
			}
		}
		
		override public function onTransitionComplete(event:TransitionEvent):void{
			startScrolling();
		}
		
		//Handlers
		private function onClickNoteButton(event:MouseEvent):void{
			if(!_staff.currentNote){
				trace("current note is null");
				return;
			}
			
			var noteName:String = event.target.name.substring(7, event.target.name.length);
			var success:Boolean = _staff.guessCurrentNoteByName(noteName);
			var color:uint = (success) ? Color.GREEN : Color.RED;
			
			_appFacade.scoreManager.currentScore += (success) ? _appFacade.SUCCESS_SCORE : _appFacade.FAILURE_SCORE;
			
			_staff.colorCurrentNote(color);
		}
	}
}