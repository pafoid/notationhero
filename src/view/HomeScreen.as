package view
{
	import com.pafoid.display.ButtonFactory;
	import com.pafoid.display.TextFieldFactory;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import events.ScreenEvent;
	
	public class HomeScreen extends Screen
	{
		private var _titleTf:TextField;
		private var _mode1Button:Sprite;
		
		public function HomeScreen(appFacade:AppFacade)
		{
			super(appFacade);
			name = ScreenConstants.HOME_SCREEN;
			
			init();
		}
		
		private function init():void{
			_titleTf = TextFieldFactory.createTextField("NotationHero", 0, 0, new TextFormat(null, 40), false, "left", this);
			_titleTf.scaleX = 1/_appFacade.scaleX;
			_titleTf.scaleY = 1/_appFacade.scaleY;
			centerHorizontaly(_titleTf);
			_titleTf.y = 100 * 1/_appFacade.scaleY;
			
			_mode1Button = ButtonFactory.createButton(null, this, "Mode 1", 200, 60, 0, 0, 40, true, clickHandler);
			_mode1Button.scaleX = 1/_appFacade.scaleX;
			_mode1Button.scaleY = 1/_appFacade.scaleY;
			center(_mode1Button);
		}
		
		//Handlers
		private function clickHandler(event:MouseEvent):void{
			switch(event.target){
				case _mode1Button:
					dispatchEvent(new ScreenEvent(ScreenEvent.CHANGE_SCREEN, ScreenConstants.MODE1_SCREEN));
					break;
			}
		}
	}
}